﻿using StudentEditor.Entities;
using StudentEditor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentEditor.DataAccess
{
    public  class StudentRepository : IRepository<Student>
    {
        public List<Student> Items { get; }
   
        private static StudentRepository _instance;

        private StudentRepository()
        {
           this.Items = new List<Student>();
        }

        public static StudentRepository Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new StudentRepository();
                }
                return _instance;
            }
        }

    }
}
