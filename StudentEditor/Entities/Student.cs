﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentEditor.Entities
{
    public class Student : BaseEntity
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
