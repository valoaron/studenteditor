﻿
using StudentEditor.Exceptions;
using StudentEditor.Interfaces;
using StudentEditor.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentEditor.Forms
{
    public partial class StudentEditor : BaseForm
    {
        IStudentService _service;
        StudentViewModel _studentModel;
        public StudentEditor(IStudentService service, StudentViewModel student)
        {
            this._service = service;
            this._studentModel = student;
            InitializeComponent();
            nameTextBox.Text = student.Name;
            birthDateDatePicker.MinDate = DateTime.Now.Date.AddYears(-25);
            birthDateDatePicker.MaxDate = DateTime.Now.Date.AddYears(-5);
            birthDateDatePicker.Value = student.BirthDate < birthDateDatePicker.MinDate ? birthDateDatePicker.MinDate : student.BirthDate;
        }

        private async void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ValidateModel();
                _studentModel.Name = nameTextBox.Text;
                _studentModel.BirthDate = birthDateDatePicker.Value.Date;
                var result = await  _service.Upsert(_studentModel.ToEntity());
                if (result == null)
                    throw new Exception();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ValidateModel()
        {
            if (String.IsNullOrEmpty(nameTextBox.Text) || String.IsNullOrWhiteSpace(nameTextBox.Text))
            {
                throw new StudentException("Name is required");
            }
            if (birthDateDatePicker.Value == null)
            {
                throw new StudentException("Birth Date is required");
            }
            if (birthDateDatePicker.Value > birthDateDatePicker.MaxDate)
            {
                throw new StudentException(String.Format("The student's birth date cannot be later than {0}", birthDateDatePicker.MaxDate.ToString()));
            }
            if (birthDateDatePicker.Value < birthDateDatePicker.MinDate)
            {
                throw new StudentException(String.Format("The student's birth date cannot be earlier than {0}", birthDateDatePicker.MinDate.ToString()));
            }
        }
    }
}
