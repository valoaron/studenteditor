﻿using StudentEditor.Exceptions;
using StudentEditor.Interfaces;
using StudentEditor.Models;
using StudentEditor.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentEditor.Forms
{
    public partial class MainForm : BaseForm
    {
        IStudentService _service;
        BindingSource _bindingSource;
        BindingList<StudentViewModel> _bindingList;
        public MainForm(IStudentService service)
        {
            this._service = service;
            InitializeComponent();
            studentsDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            LoadData(null);
        }

        private async void LoadData(string nameFilter)
        {
            try
            {
                studentsDataGrid.DataSource = null;

                var students = await _service.GetStudents(nameFilter);
                _bindingList = new BindingList<StudentViewModel>();
                if (students != null && students.Count > 0)
                {
                    students.ForEach(x => _bindingList.Add(new StudentViewModel(x)));
                }
                _bindingSource = new BindingSource();
                _bindingSource.DataSource = _bindingList;
                studentsDataGrid.DataSource = _bindingSource;
                studentsDataGrid.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void addNew_Click(object sender, EventArgs e)
        {
            try
            {
                UpsertStudent(new StudentViewModel());
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedStudent = GetSelectedStudent();
                UpsertStudent(selectedStudent);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

        }

        private void UpsertStudent(StudentViewModel model)
        {
                var result = new StudentEditor(_service, model).ShowDialog();
                if (result == DialogResult.OK)
                    LoadData(null);
        }


        private async void deleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedStudent = GetSelectedStudent();
                await _service.Delete(selectedStudent.ID);
                studentsDataGrid.Rows.RemoveAt(studentsDataGrid.SelectedRows[0].Index);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            LoadData(searchBox.Text);
        }

        private StudentViewModel GetSelectedStudent()
        {
            if (studentsDataGrid != null && studentsDataGrid.SelectedRows != null && studentsDataGrid.SelectedRows.Count > 0)
            {
                var selectedItem = studentsDataGrid.SelectedRows[0].DataBoundItem as StudentViewModel;
                return selectedItem;
            }
            throw new StudentException("No student was selected!");
        }

    }
}
