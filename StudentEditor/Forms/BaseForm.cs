﻿using StudentEditor.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentEditor.Forms
{
   public  class BaseForm : Form
    {
        protected void HandleException(Exception ex)
        {
            Debug.Write(ex.Message);
            if (ex is StudentException)
            {
                MessageBox.Show(ex.Message);
            }
            else
            {
                MessageBox.Show("Error occured");
            }
            
        }
    }
}
