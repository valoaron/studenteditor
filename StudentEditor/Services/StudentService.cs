﻿using StudentEditor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentEditor.Entities;
using StudentEditor.DataAccess;
using StudentEditor.Exceptions;

namespace StudentEditor.Services
{
    public class StudentService : IStudentService
    {

        public Task<List<Student>> GetStudents(string name)
        {
            return Task.Run(() =>
            {
                var nameFilter = String.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name) ? null : name.ToLowerInvariant().Trim();
                return StudentRepository.Instance.Items.Where(x => nameFilter == null || x.Name.ToLowerInvariant().Contains(nameFilter)).ToList(); 
            });
        }

        public  Task<bool> Delete(int studentID)
        {
           return  Task.Run(async () =>
            {
                var toBeRemoved =await GetStudentByID(studentID);
                StudentRepository.Instance.Items.Remove(toBeRemoved);
                return true;
            });
        }

        public Task<Student> Upsert(Student student)
        {
            return Task.Run(async () =>
            {
                Student result = null;
                var existingStudentByName = await GetStudents(student.Name);
                if (existingStudentByName != null
                        && (existingStudentByName.Count > 1
                            || (existingStudentByName.Count == 1 && existingStudentByName[0].ID != student.ID)))
                {
                    throw new StudentException(String.Format("Student already exists with the given name ({0})!", student.Name));
                }

                if(student.ID != 0)
                {
                    var existingStudent =await  GetStudentByID(student.ID);
                    existingStudent.LastModifiedDate = DateTime.Now;
                    existingStudent.BirthDate = student.BirthDate;
                    existingStudent.Name = student.Name;
                    result = existingStudent;
                }
                else
                {
                    student.CreatedDate = DateTime.Now;
                    student.ID = Guid.NewGuid().GetHashCode();
                    StudentRepository.Instance.Items.Add(student);
                    result = student;
                }
                return result;
            });
        }

        public Task<Student> GetStudentByID(int studentID)
        {
            return Task.Run(() =>
            {
                var student = StudentRepository.Instance.Items.SingleOrDefault(x => x.ID == studentID);
                if (student == null)
                    throw new StudentException(String.Format("No student was found with the given ID ({0})!", studentID));
                else
                    return student;
            });
        }
    }
}
