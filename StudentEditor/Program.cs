﻿using SimpleInjector;
using StudentEditor.DataAccess;
using StudentEditor.Entities;
using StudentEditor.Forms;
using StudentEditor.Interfaces;
using StudentEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentEditor
{
    static class Program
    {
        private static Container _container;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigureContainer();
            Application.Run(_container.GetInstance<MainForm>());
        }

        private static void ConfigureContainer()
        {
            _container = new Container();
            _container.Register<IStudentService, StudentService>();
            _container.Register<MainForm>();
        }
    }
}
