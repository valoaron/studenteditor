﻿using StudentEditor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentEditor.Models
{
    public class StudentViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        [System.ComponentModel.DisplayName("Date of birth")]
        public DateTime BirthDate { get; set; }

        [System.ComponentModel.DisplayName("Created date")]
        public string CreatedDate { get; set; }

        [System.ComponentModel.DisplayName("Modified date")]
        public string LastModifiedDate { get; set; }

        public StudentViewModel()
        {

        }

        public StudentViewModel(Student student)
        {
            this.Name = student.Name;
            this.ID = student.ID;
            this.BirthDate = student.BirthDate;
            if (student.CreatedDate != null)
                this.CreatedDate = student.CreatedDate.ToString();
            if (student.LastModifiedDate != null)
                this.LastModifiedDate = student.LastModifiedDate.ToString();
        }

        public Student ToEntity()
        {
            var result = new Student();

            result.ID = this.ID;
            result.Name = this.Name;
            result.BirthDate = this.BirthDate;
            if (this.LastModifiedDate != null)
                result.LastModifiedDate = DateTime.Parse(this.LastModifiedDate);
            if (this.CreatedDate != null)
                result.CreatedDate = DateTime.Parse(this.CreatedDate);

            return result;
        }

    }

}
