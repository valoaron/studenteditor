﻿using StudentEditor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentEditor.Interfaces
{
    public interface IStudentService
    {
        Task<Student> Upsert(Student student);
        Task<bool> Delete(int studentID);

        Task<Student> GetStudentByID(int studentID);

        Task<List<Student>> GetStudents(string name);
    }
}
